import Control.Monad.State
import Data.List (intercalate)
import System.Environment (getArgs)
import qualified System.Console.ANSI as Console
import Control.Concurrent (threadDelay)

type Screen = [String]
type Position = (Int, Int)
type Snake = [Position]
type Fruit = Position
type Piece = Char
data Direction = UP | DOWN | RIGHT | LEFT
data GamePiece = SnakePiece | FruitPiece | WallPiece

instance Show GamePiece where
    show p = case p of
        SnakePiece -> "#"
        FruitPiece -> "o"
        WallPiece  -> "-"

data GameState = GameState
    { snake     :: Snake
    , fruit     :: Fruit
    , direction :: Direction
    , width     :: Int
    , height    :: Int }

moveSnake :: Snake -> Direction -> Snake
moveSnake [] _ = []
moveSnake snake direction
    = case direction of
        UP    -> moveTo x (y-1)
        DOWN  -> moveTo x (y+1)
        RIGHT -> moveTo (x+1) y
        LEFT  -> moveTo (x-1) y
        where
            (x, y) = head snake
            moveTo x' y' = (x', y') : (reverse . tail . reverse $ snake)

paintPixel :: Position -> Piece -> Screen -> Screen
paintPixel pos piece screen = up ++ (row : tail down)
    where
        (x, y) = pos
        (up, down) = splitAt y screen
        (left, right) = splitAt x $ head down
        row = left ++ (piece : tail right)

paintSnake :: Snake -> Screen -> Screen
paintSnake [] screen = screen
paintSnake (p:ps) screen = paintSnake ps screen'
    where
        screen' = paintPixel p (head . show $ SnakePiece) screen

paintFruit :: Fruit -> Screen -> Screen
paintFruit p screen = paintPixel p (head . show $ FruitPiece) screen

paintGame :: GameState -> Screen
paintGame g = (paintFruit $ fruit g) . (paintSnake $ snake g) $ clearArena (width g) (height g)
        
putScreen :: Screen -> IO ()
putScreen screen = do
    Console.setCursorPosition 0 0
    --Console.clearLine
    putStrLn . intercalate "\n" $ screen

initGame :: GameState
initGame = GameState (defaultSnake 20) defaultFruit defaultDirection defaultWidth defaultHeight
    where
        defaultSnake n   = reverse [(x+20,15) | x <- [0..(n-1)]]
        defaultFruit     = (1,4)
        defaultDirection = RIGHT
        defaultWidth     = 60
        defaultHeight    = 30

clearArena :: Int -> Int -> Screen
clearArena w h = [['.' | _ <- [0..w]] | _ <- [0..h]]

processInput :: [Direction] -> StateT GameState IO ()
processInput directions = mapM_ move directions

draw :: GameState -> IO ()
draw game = do
    Console.clearScreen
    putScreen . paintGame $ game
    threadDelay $ ceiling (1000000 / 20)

move :: Direction -> StateT GameState IO ()
move direction = do
    modify $ \state -> state { snake = moveSnake (snake state) direction }
    state <- get
    liftIO $ draw state

snakeGame :: [Direction] -> IO ()
snakeGame dirs = evalStateT (processInput dirs) initGame

main :: IO ()
main = do
    (arg:args) <- getArgs
    snakeGame . take (read arg :: Int) . cycle $ [DOWN, DOWN, DOWN, RIGHT, RIGHT, RIGHT, UP, UP, RIGHT, RIGHT, DOWN, DOWN, DOWN, DOWN, LEFT, LEFT, LEFT, LEFT, LEFT, LEFT, LEFT, LEFT, LEFT, LEFT, LEFT, LEFT, LEFT, LEFT, LEFT, LEFT, LEFT, LEFT, LEFT, LEFT, LEFT, LEFT, LEFT, UP, UP, UP, UP, UP, UP, UP, UP, UP, RIGHT, RIGHT, RIGHT]
